#!/usr/bin/python3
#
# Author: Julien Marin - projet Afrikalan  
# Contact: julien.marin@afrikalan.org
# Licence: GPL v3.0
#
# Usage: zimCutIndex.py inputFile.zim outputFile.zim
# (using this script as is will makes outputFile.zim a copy of inputFile.zim, without the index.)
# 
# 

import urllib.request
import urllib.parse
import urllib.error
import sys
from lxml import html
import base64
import mimetypes
from pprint import pprint
import re


def safeAndBeautifullFilename(name):
  reAllowed=re.compile(r"[0-9a-zA-z_.-]")
  dictRepl={'é': 'e',   'è': 'e',   'ê': 'e',   'É': 'E',   'È': 'E',   'Ê': 'E',
            'à': 'a',   'â': 'a',   'À': 'A',   'Â': 'A',   
            'ù': 'u',   'û': 'u',   'Ù': 'U',   'Û': 'U',   
            'ô': 'o',   'Ô': 'O',
            '—': '-'
            }
  
  name=re.sub(r'\.+', '.', name) #make sure there is no ..
  
  retName=''
  for i in range(len(name)):
    car=name[i]
    if not reAllowed.match(car):
      if dictRepl.get(car) is not None:
        car=dictRepl.get(car)
      else:
       car='_'
    retName+=car
  
  return(retName)


class Embedoc:
  def __init__(self, url):
    self.url=url

    self.xmlRoot=html.fromstring(self.fetchUrl(self.url).decode("utf8"))

  def getTitle(self):
    arTitles=self.xmlRoot.findall("./head/title")
    if (len(arTitles)==0):
      return("sans titre")
    else:
      return(arTitles[0].text)

  def absUrl(self, relurl):
    return(urllib.parse.urljoin(self.url, relurl))

  def fetchUrl(self, url):
    scheme, netloc, path, query, fragment = urllib.parse.urlsplit(url)
    
    if urllib.parse.unquote(path)==path: #check if URL already encoded
      path = urllib.parse.quote(path)
    url = urllib.parse.urlunsplit((scheme, netloc, path, query, fragment))
    print("Fetching "+url+"...");
    
    req = urllib.request.Request(
      url,
      headers={'User-Agent': 'Afrikalan/3.2 getEmbed.py, https://gitlab.com/Afrikalan/various_tools, http://afrikalan.org'}
    )
    
    try:
      fp = urllib.request.urlopen(req)
      http_retCode=fp.getcode()
      if http_retCode>=400:
        return False
      return(fp.read())
    except urllib.error.HTTPError:
      return False


  def embedImages(self):
    print("\nEmbedding Images:")
    for imageNode in self.xmlRoot.iter("img"):
      src=imageNode.attrib.get('src')
      if src is not None:
        #TODO: check if URL should be downloaded (ie: not already a data url)
        imgContent=self.fetchUrl(self.absUrl(src))
        if imgContent is False:
          continue
        
        mime=mimetypes.guess_type(self.absUrl(src), strict=False)[0]
        if mime is None:
          mime="image/png"
        
        imageNode.attrib['src']='data:'+mime+';base64,'+base64.b64encode(imgContent).decode()
        
        if imageNode.attrib.get('srcset') is not None:
          del imageNode.attrib['srcset']

  def searchContainer(self, node, arclasses, maxlev=4, uplevel=0):
    if uplevel>maxlev:
      return(False)
    try: 
      parent=node.getparent()
      classes=parent.get('class')
      if classes is None:
        classes=''
    except:
      pass
      return(False)
    
    
    #print('#'+("-"*uplevel)+classes)
    for actClass in classes.split(" "):
      if actClass in arclasses:
        #print ("FOUND: "+classes)
        return(parent)
    #nothing found searching one level up
    return (self.searchContainer(parent, arclasses, uplevel=uplevel+1))


  def removeImages(self, wikiContainerRemove=False):
    print("\nRemoving Images:")
    for imageNode in self.xmlRoot.iter("img"):
      target=False
      
      if wikiContainerRemove:
        target=self.searchContainer(imageNode, ["thumbinner", "gallerybox"])
      if target is False:
        target=imageNode
        
      target.drop_tree()
      #target.getparent().remove(target) #this one causes loss of text
      
      #p=target.getparent()
      #print("###"+str(p.text))
      #print("^^^"+str(p.text))


  def embedCss(self):
    headNode=self.xmlRoot.find("./head")
    print("\nEmbedding CSS:")
    for styleNode in self.xmlRoot.iter("link"):
      if styleNode.attrib.get("rel")=="stylesheet":
        contentNodec=self.fetchUrl(self.absUrl(styleNode.attrib["href"]))
        if contentNodec is False:
          continue
        else :
          newNode=html.Element("style")
          newNode.attrib["type"]="text/css"
          newNode.text=contentNodec.decode("utf8")
          headNode.append(newNode)
          
          styleNode.getparent().remove(styleNode)
        
        

  def embedJs(self):
    print("\nEmbedding javascript:")
    for jsNode in self.xmlRoot.iter("script"):
      if jsNode.attrib.get("src") is not None:
        contentNodec=self.fetchUrl(self.absUrl(jsNode.attrib["src"]))
        if contentNodec is False:
          continue
        else :
          jsNode.text=contentNodec.decode("utf8")
          del jsNode.attrib['src']



  def correctLinks(self):
    print("\nCorrecting links:")
    for linkNode in self.xmlRoot.iter("a"):
      href=linkNode.attrib.get("href")
      if href is not None and len(href)>0:
        if linkNode.attrib.get("href")[0]!="#":
          fullUrl=urllib.parse.urljoin(self.url, linkNode.attrib["href"])
         
          splitAnchor=fullUrl.split('#')
          if splitAnchor[0]==self.url and len(splitAnchor)>1:
            fullUrl='#'+splitAnchor[1]
          
          linkNode.attrib["href"]=fullUrl


  def removeKiwixBar(self):
    kiwixBarParentResult=self.xmlRoot.xpath("//span[@class='kiwix']")
    if len(kiwixBarParentResult)!=0:
      kiwixBarParentNode=kiwixBarParentResult[0]
      kiwixBarResult=kiwixBarParentNode.xpath("span[@id='kiwixtoolbar']")
      if len(kiwixBarResult)!=0:
        print("\nKiwix bar found: will now suppress it...")
        
        #kiwix put a div with fixed height after the parent, we search and remove that
        parentNext=kiwixBarParentNode.getnext()
        if parentNext.attrib.get('style') is not None and 'height:' in parentNext.attrib.get('style'):
          parentNext.getparent().remove(parentNext)
        
        #now let's suppress the parent node
        kiwixBarParentNode.getparent().remove(kiwixBarParentNode)

  def removeBase(self):
    resultsBase=self.xmlRoot.xpath("//base")
    for result in resultsBase:
      result.getparent().remove(result)
      print('tag <base> supprimé')
      
  #usefull when dumping Wikipedia to PDF
  def detailsToDiv(self): 
    for elt in self.xmlRoot.xpath(".//details"):
      elt.tag="div"
    print('tags <details> changés en <div>')

  def filename(self, ext=True):
    fileName=self.getTitle()
    if ext:
      fileName+=".html"
    return(safeAndBeautifullFilename(fileName))

  def getXmlRoot(self):
    return(self.xmlRoot)

  def saveAsString(self):
    return(html.tostring(self.xmlRoot).decode("utf8"))

  def save(self, fileName):
    print("\nSaving to "+fileName+"...")
    fp=open(fileName, "wb")
    fp.write(html.tostring(self.xmlRoot))
    fp.close()



def main():
  if len(sys.argv)!=2:
      print ("usage: ", sys.argv[0], "url_to_html_doc")
      sys.exit(1) 
      
  objEmbedoc=Embedoc(sys.argv[1])
  objEmbedoc.embedImages()
  objEmbedoc.embedCss()
  objEmbedoc.correctLinks()
  objEmbedoc.removeBase()
  objEmbedoc.removeKiwixBar()
  objEmbedoc.save(objEmbedoc.filename())

if __name__ == "__main__":
  main()


